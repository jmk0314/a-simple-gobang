#ifndef _button
#define _button

#include <easyx.h>
#include <cstring>

using namespace std;

class button {
public:
	//绘图函数
	virtual void print() = 0;
	//捕获用户信息,进行处理函数
	virtual bool action(ExMessage& inp) = 0;
};
#endif // !button