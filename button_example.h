#ifndef _button_example
#define _button_example

#include "button.h"
#include "cboard.h"
#include <easyx.h>
#include <iostream>
#include "AI.h"
#include "resource.h"
using namespace std;

class rectangle_button :public button {
private:
	int l, u, r, d;
	int tx, ty;
	COLORREF colorline;
	COLORREF colorchar;
	int charsize;
	char* str;
public:
	rectangle_button(int l, int u, int r, int d, const char* inp, int tx, int ty, COLORREF clrline, COLORREF chrline, int size) :l(l), u(u), r(r), d(d), tx(tx), ty(ty) {
		int len = strlen(inp);
		len++;
		str = new char[len];
		strcpy_s(str, len, inp);
		colorline = clrline;
		colorchar = chrline;
		charsize = size;
	}
	~rectangle_button() {
		delete[] str;
	}
	//打印画面
	virtual void print() override {
		setlinecolor(colorline);
		settextcolor(colorchar);
		rectangle(l, u, r, d);
		settextstyle(charsize, 0, _T("隶书"));
		outtextxy(l + tx, u + ty, (const char*)str);
		//cout << str << endl;
	}
	//捕获用户输入
	virtual bool action(ExMessage& inp) override {
		if (inp.x >= l && inp.x <= r && inp.y <= d && inp.y >= u && inp.message == WM_LBUTTONDOWN) {
			inp.message = NULL;
			return true;
		}
		return false;
	}
};


class p_title :public button {
public:
	void print() {
		settextstyle(50 , 0, _T("隶书"));
		TCHAR s[] = _T("五 子 棋");
		outtextxy(250, 100, s);
	}

	bool action(ExMessage& inp) {
		return false;
	}
};


class b_Start :public rectangle_button {
public:
	b_Start() : rectangle_button(250, 250, 450, 310, "开始游戏", 20, 10, RGB(0, 0, 0), RGB(0, 0, 0), 40) {}
};

class b_About :public rectangle_button {
public:
	b_About() : rectangle_button(250, 380, 450, 440, "制作信息", 20, 10, RGB(0, 0, 0), RGB(0, 0, 0), 40) {}
};

class p_About :public button {
private:
	IMAGE img1, img2, img3, img4;
public:
	p_About() {
		loadimage(&img1, _T("JPG"), MAKEINTRESOURCE(IDB_BITMAP1), 100, 100);
		loadimage(&img2, _T("JPG"), MAKEINTRESOURCE(IDR_JPG1), 100, 100);
		loadimage(&img3, _T("JPG"), MAKEINTRESOURCE(IDR_JPG2), 100, 100);
		loadimage(&img4, _T("JPG"), MAKEINTRESOURCE(IDR_JPG3), 100, 100);
	}
	void print() {
		settextcolor(RGB(0,0,0));
		settextstyle(30, 0, _T("隶书"));

		putimage(200, 100, &img1);
		outtextxy(230, 200, "JMK");

		putimage(400, 100, &img2);
		outtextxy(430, 200, "LHJ");

		putimage(200, 300, &img3);
		outtextxy(230, 400, "LJC");

		putimage(400, 300, &img4);
		outtextxy(430, 400, "HEM");

		settextstyle(40, 0, _T("隶书"));
		TCHAR s[] = _T("制 作 人 员");
		outtextxy(240, 500, s);

	}
	bool action(ExMessage& inp) {
		return false;
	}
};

class p_win :public button {
public:
	void print() {
		settextcolor(RGB(0, 0, 0));
		settextstyle(40, 0, _T("隶书"));
		outtextxy(290, 625, "你赢了");
	}
	bool action(ExMessage& inp) {
		return false;
	}
};

class p_lose :public button {
public:
	void print() {
		settextcolor(RGB(0, 0, 0));
		settextstyle(40, 0, _T("隶书"));
		outtextxy(290, 625, "你输了");
	}
	bool action(ExMessage& inp) {
		return false;
	}
};

class b_Exit :public rectangle_button {
public:
	b_Exit() :rectangle_button(650, 10, 695, 30, "退出", 1, 1, RGB(0, 0, 0), RGB(0, 0, 0), 19) {}

};


checkerboard cboard;
class b_Cboard :public button {
private:
	//棋盘点距
	const int cblen = 40;
	//显示棋盘时棋盘的左上角点
	const int cbx = 70, cby = 50;
	//棋盘宽度
	const int cbmaxlen = cblen * 14;
	//显示落子位置
	int tx = 0, ty = 0;

	//设置颜色
	const int player = 1;
	const int AI = 2;
	
	//结束标志
	int flag = 0;

	int markx = -1, marky = -1;

	//画棋盘
	void printcboard() {
		setlinecolor(BLACK);
		for (int i = 0; i < 15; i++) {
			line(cbx, cby + i * cblen, cbx + cbmaxlen, cby + i * cblen);
		}
		for (int i = 0; i < 15; i++) {
			line(cbx + i * cblen, cby, cbx + i * cblen, cby + cbmaxlen);
		}
	}

	//画棋子
	void printcheck() {
		setlinecolor(BLACK);
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				if (cboard.getcheck(i, j) == 1) {
					setlinecolor(BLACK);
					setfillcolor(BLACK);
					fillcircle(cbx + cblen * i, cby + cblen * j, 10);
				}
				else if (cboard.getcheck(i, j) == 2) {
					if (markx == j && marky == i) setlinecolor(RGB(180, 65, 85));
					else setlinecolor(BLACK);
					setfillcolor(WHITE);
					fillcircle(cbx + cblen * i, cby + cblen * j, 10);
				}
			}
		}

	}

	//画落子提示棋
	void printcheck(int x, int y) {
		if (x < 0 || y < 0) return;
		setlinecolor(RGB(0, 0, 0));
		setfillcolor(RGB(192, 192, 192));
		fillcircle(cbx + cblen * x, cby + cblen * y, 10);
	}


public:
	void print() {
		this->printcboard();
		this->printcheck();
		this->printcheck(tx, ty);
	}

	void clear() {
		flag = 0;
	}
		
	int getwiner() {
		return flag;
	}

	bool action(ExMessage& inp) {

		if (flag != 0) {
			//棋局已经结束，不处理任何消息
			return true;
		}
		if (inp.message == WM_LBUTTONDOWN && inp.lbutton == true) {
			inp.message = NULL;
			int x = inp.x + cblen / 2;
			int y = inp.y + cblen / 2;
			if (x >= cbx && x <= cbx + cbmaxlen + cblen && y >= cby && y <= cby + cbmaxlen + cblen) {
				x -= cbx;
				if (x < 0) {
					x = 0;
				}
				else {
					x /= cblen;
				}
				y -= cby;
				if (y < 0) {
					y = 0;
				}
				else {
					y /= cblen;
				}
				if (cboard.getcheck(x, y) == 0) {
					if (cboard.putcheck(x, y)) {
						flag = player;
						tx = -1;
						ty = -1;
						return true;
					}
					//坐标反了
					AIplayer.playerinp(y, x);
					AIplayer.getAIinp(markx, marky);
					if (cboard.putcheck(marky, markx)) {
						flag = AI; 
						tx = -1;
						ty = -1;
						return true;
					}
				}

			}
		}else{
			//显示落子位置提示
			int x = inp.x + cblen / 2;
			int y = inp.y + cblen / 2;
			if (x >= cbx && x <= cbx + cbmaxlen + cblen && y >= cby && y <= cby + cbmaxlen + cblen) {
				x -= cbx;
				if (x <= 0) {
					x = 0;
				}
				else {
					x /= cblen;
				}
				y -= cby;
				if (y <= 0) {
					y = 0;
				}
				else {
					y /= cblen;
				}
				if (cboard.getcheck(x, y) == 0) {
					tx = x;
					ty = y;
				}
				else {
					tx = -1;
					ty = -1;
				}
			}
			else {
				tx = -1;
				ty = -1;
			}
		}
		return false;
	}
};
#endif // !_button_example