#ifndef _page_example

#define _page_example


#include "page.h"
#include "button_example.h"
using namespace std;

class main_page :public Page {
public:
	b_Start startt;
	b_About aboutt;
	p_title title;
	b_Exit exitt;
	void print() {
		exitt.print();
		title.print();
		startt.print();
		aboutt.print();
	}
	void action(ExMessage& inp);
}main_page;


class gameing_page :public Page {
public:
	b_Exit exitt;
	b_Cboard board;
	p_win a;
	p_lose b;
	int flag = 0;
	void print() {
		if (flag != 0) {
			if (flag == 1) {
				a.print();
			}
			else {
				b.print();
			}
		}
		exitt.print();
		board.print();
	}
	void action(ExMessage& inp);
}gameing_page;


class about_page :public Page {
public:
	b_Exit exitt;
	p_About about;
	void print() {
		exitt.print();
		about.print();
	}
	void action(ExMessage& inp);
}about_page;


void main_page::action(ExMessage& inp) {
	if (startt.action(inp)) {
		main_window.inp_page(&gameing_page);
	}
	else if (aboutt.action(inp)) {
		main_window.inp_page(&about_page);
	}
	else if (exitt.action(inp)) {
		main_window.close();
	}
}

void gameing_page::action(ExMessage& inp) {
	
	if (exitt.action(inp)) {
		main_window.inp_page(&main_page);
		cboard.clear();
		AIplayer.clear();
		board.clear();
		flag = 0;
		return;
	}
	if (board.action(inp)) {
		flag = board.getwiner();
	}
	
}

void about_page::action(ExMessage& inp) {
	if (exitt.action(inp)) {
		main_window.inp_page(&main_page);
	}
}
#endif // !_page_example