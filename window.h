#ifndef _window
#define _window

#include <iostream>
#include <easyx.h>
#include "page.h"
#include "button.h"

//背景音乐API
#include <mmsystem.h>
#pragma comment(lib,"winmm.lib")
#include "resource.h"

class window {
private:
	int x;
	int y;         //窗口大小 x y;
	bool flag;     //防止多次调用open或close
	IMAGE img;     //背景图片
public:
	//窗口大小 x y;
	Page* it = NULL;
	ExMessage inp;
	window(int x, int y) :x(x), y(y), flag(false) {
		//构造函数初始化
		//加载背景音乐
		PlaySound(MAKEINTRESOURCE(IDR_WAVE1), NULL, SND_RESOURCE | SND_ASYNC | SND_LOOP);
		//加载背景图片
		loadimage(&img, _T("JPG"), MAKEINTRESOURCE(IDR_JPG4), 700, 700);
	}
	bool is_open() {
		return flag;
	}
	void open() {
		//显示页面
		if (flag) return;
		flag = true;
		it = NULL;
		initgraph(x, y);
		setbkcolor(WHITE);
		setbkmode(TRANSPARENT);
		cleardevice();
	}
	void close() {
		//关闭页面
		if (!flag) return;
		flag = false;
		closegraph();
	}
	~window() {
		//析构函数
		close();
	}
	void inp_page(Page* inp) {
		//重新填装页面
		it = inp;
	}
	void refresh();      //刷新页面
	void getmessage();   //捕获消息并传递到页面中
}main_window(700, 700);


void window::refresh() {
	//刷新页面
	BeginBatchDraw();
	cleardevice();
	putimage(0, 0, &img);
	it->print();
	EndBatchDraw();
}

void window::getmessage()
{
	//消息获取函数
	inp.message = NULL;
	peekmessage(&inp, -1, true);
	it->action(inp);
}


#endif // !_window
