#ifndef _page
#define _page
#include <iostream>
#include <easyx.h>
#include "window.h"
#include "button.h"
#include <vector>

class Page {
public:
	//显示页面函数
	virtual void print() = 0;
	//将消息传递给页面上每一个按钮
	virtual void action(ExMessage& inp) = 0;
};

#endif // !_page