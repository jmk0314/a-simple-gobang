#pragma once
#include <iostream>
#include <easyx.h>
#include "button.h"
#include "window.h"
#include "page.h"
#include "cboard.h"
#include "page_example.h"
#include "button_example.h"
using namespace std;

int main() {
	main_window.open();
	main_window.inp_page(&main_page);
	while (main_window.is_open()) {
		main_window.refresh();
		main_window.getmessage();
	}
	return 0;
}