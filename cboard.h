#ifndef _cboard
#define _cboard

#include <iostream>

using namespace std;

class checkerboard {
private:
	int biao[15][15];
	int flag = 1;

	//判断棋子是否越界
	bool yuejie(int x, int y)const {
		if (x < 0 || x>14 || y < 0 || y>14) return true;
		else return false;
	}

	//判断一个方向上是否已经出现五子
	bool isfinish(const int x, const int y, const int xf, const int yf) const {
		int num = 1;
		int player = biao[x][y];
		int tx = x, ty = y;
		for (int i = 1; i < 5; i++) {
			if (yuejie(tx, ty) || num >= 5) {
				break;
			}
			if (biao[tx + xf][ty + yf] == player) {
				num++;
				tx += xf;
				ty += yf;
			}
			else {
				break;
			}
		}
		tx = x, ty = y;
		for (int i = 1; i < 5; i++) {
			if (yuejie(tx, ty) || num >= 5) {
				break;
			}
			if (biao[tx - xf][ty - yf] == player) {
				num++;
				tx -= xf;
				ty -= yf;
			}
			else {
				break;
			}
		}
		if (num >= 5) {
			cout << "wocccccc!\n" << player << " " << num << "???\n";
			//system("pause");
			return true;
		}
		else return false;
	}


	//判断棋局是否已经结束
	bool judge(int x, int y) {
		if (isfinish(x, y, 1, 0)) return true;
		if (isfinish(x, y, 0, 1)) return true;
		if (isfinish(x, y, 1, 1)) return true;
		return isfinish(x, y, 1, -1);
	}

public:
	checkerboard() {
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				biao[i][j] = 0;
			}
		}
	}


	//放入棋子
	bool putcheck(int x, int y) {
		biao[x][y] = flag;
		flag %= 2;
		flag += 1;
		return judge(x, y);
	}
	//获得棋子
	int getcheck(int x, int y) {
		//0为空
		//1为P1
		//2为P2
		return biao[x][y];
	}
	//清理棋盘
	void clear() {
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				biao[i][j] = 0;
			}
		}
		flag = 1;
	}
};


#endif // !cboard